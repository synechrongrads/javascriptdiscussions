String          - '' and "" as string
Number          - Integer, Decimal, Exponential Notation
BigInt          - Long values
Boolean         - true, false
Undefined       - whenever there is a variable without a value then it is considered as Undefined
Null            - no refernece to the object that the variable is referring to
Symbol          - To convert something to as Symbol ( uniqueness)
Object          - An Object literal
                - An Array
                - Date


Functions:

    function functionName(param1, param2, param......){

    }

    