// Creating the object using Literals
employee = {id:123123,name:'Gajanan',salary:'67890 USD per month'}
console.log("Employee Id: " + employee.id);
console.log("Employee Name: " + employee.name);
console.log("Employee Salary: " + employee.salary);


//Creating object using the Object instance
var emp = new Object();
emp.id = 234234;
emp.name = "Shiraz";
emp.salary = "56654 CAD per month";

console.log("Employee Id: " + emp.id);
console.log("Employee Name: " + emp.name);
console.log("Employee Salary: " + emp.salary);

//Creating object using the object's constructor

function candidates(id, name, salary, course){
    this.id = id;
    this.name = name;
    this.salary = salary;
    this.course = course;
}

cand = new candidates(100,"Muthu","100 Rs","Javascript");
console.log("Candidate Id: " + cand.id);
console.log("Candidate Name: " + cand.name);
console.log("Candidate Salary: " + cand.salary);
console.log("Candidate Course: " + cand.course);