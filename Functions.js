//Hoisting

FunctionOne();
FunctionTwo(12);
mystmt = FunctionThree();
console.log(mystmt);
console.log(FunctionFour("Good Evening Gajanan"));


console.log("Function Type 1: No return value, no input")

function FunctionOne(){
    console.log("Function Type 1");
}

console.log("Function Type 2: No return value, some input")

function FunctionTwo(val){
    console.log("Function Type 2 - value: " + val);
}

console.log("Function Type 3: return value, no input")

function FunctionThree(){
    return "Hello, world!";
}

console.log("Function Type 4: return value, some input")

function FunctionFour(vals){
    return vals;
}

function GetAttendeeNames(){
    let part1 = 'Conor', part2 = 'Shiraz', part3 = 'Mehrsa', part4 = 'Gajanan', part5 = 'Muthu';
    return {part1, part2, part3, part4,part5}
}

let names = GetAttendeeNames();
for (let i in names) {
    console.log(names[i]);
}
console.log("\n")
let {part1,part2,part3,part4,part5} = GetAttendeeNames();
console.log(part1);
console.log(part2);
console.log(part3);
console.log(part4);
console.log(part5);