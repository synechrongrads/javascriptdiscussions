//Array Literals

var rainbow_colors = ['Violet', 'Indigo', 'Blue', 'Green', 'Yellow', 'Orange', 'Red'];
for(i=0; i<rainbow_colors.length; i++){
    console.log(rainbow_colors[i]);
}

//Using New Keyword
var my_fav_flowers = new Array();
my_fav_flowers[0] = 'Rose';
my_fav_flowers[1] = 'Tulip';
my_fav_flowers[2] = 'Daffodil';

for (j=0; j<my_fav_flowers.length; j++){
    console.log(my_fav_flowers[j]);
}


// using Array constructor

my_primes = new Array(2,3,5,7,97);
for(ele=0; ele<my_primes.length; ele++){
    console.log(my_primes[ele] +" ");
}